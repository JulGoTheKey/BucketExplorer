from flask import Flask, render_template, jsonify, request
import requests
import logging
import json
from google.cloud import storage
from flask_cors import CORS, cross_origin


app = Flask(__name__)
CORS(app)

@app.route('/')
def hello_world():
    return render_template('index.html')

@app.route('/ice')
def ice():
    client = storage.Client(project="fr-ist-vwt-webengineering-dev")
    bucket = client.get_bucket('test-bucket-jd')
    blob = storage.Blob('test3.txt', bucket)
    content = blob.download_as_string()
    print(content)

@app.route('/fire')
def fire():
    storage_client = storage.Client(project="fr-ist-vwt-webengineering-dev")
    bucket = storage_client.get_bucket('test-bucket-jd')

    blobs = bucket.list_blobs()
    for blob in blobs:
        print(blob.name)

    return render_template('result.html')

def download_blob(bucket_name, source_blob_name, destination_file_name):
    """Downloads a blob from the bucket."""
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(source_blob_name)


    with open(destination_file_name, 'wb') as file_obj:
        blob.download_to_file(file_obj)
#        blob.download_to_filename(destination_file_name)
        file_obj.close()

    print('Blob {} downloaded to {}.'.format(
        source_blob_name,
        destination_file_name))

@app.route('/download')
def download():
#    storage_client = storage.Client(project="fr-ist-vwt-webengineering-dev")
#   bucket = storage_client.get_bucket('test-bucket-jd')
#    blob = bucket.blob('test3.txt')
    download_blob("test-bucket-jd", "test3.txt", "C:\download_bucket\myfileeey.txt")
    return render_template('result.html')

@app.route('/getArbo', methods=['POST', 'GET'])
def getArbo():
    filesContainer = []
    storage_client = storage.Client(project="fr-ist-vwt-webengineering-dev")
    bucket = storage_client.get_bucket('archive-di')
    prefix = request.args.get('prefix')
    print('the prefix :: ' + str(prefix))
    delimiter='/'

 #   prefix='MainFolder/'
 #   delimiter='/'

    blobs = bucket.list_blobs(prefix=prefix, delimiter=delimiter)

    print('Blobs:')
    for blob in blobs:
        print(blob.name)
        blobName = blob.name
        blobNamePos = blobName.rfind('/') + 1
        filesContainer.append({"fullName": blob.name, "name": blobName[blobNamePos:], "type": "file"})

    #Affichage des sous dossiers
    if delimiter:
        print('Prefixes:')
        for folder in blobs.prefixes:
            print(folder)
            if prefix == "":
                filesContainer.append({"fullName": folder, "name": folder, "type": "folder"})
            else:
                prefixNamePos = folder[:-1].rfind('/') + 1
                filesContainer.append({"fullName": folder, "name": folder[prefixNamePos:], "type": "folder"})

    print(filesContainer)
#    return render_template('result.html')
    return jsonify(filesContainer)

if __name__ == '__main__':
    app.run(port=5443, debug=True)

#SET FLASK_DEBUG=1